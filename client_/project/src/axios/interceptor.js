import Vue from "vue";
export default {
  request(request) {
    // Do something before request is sent
    return request;
  },

  requestError(error) {
    // Do something with request error
    return Promise.reject(error);
  },

  response(response) {
    // console.log('response : ', response);
    // Any status code that lie within the range of 2xx cause this function to trigger
    // Do something with response data
    return response;
  },

  responseError(error) {
    // const errors = error.response.data.errors;
    // const error = error.response.message; 
    // console.log(errors);
    //   if (errors) {
    //     if (errors.username) {
    //       Vue.$toast.open({
    //         message: errors.username[0],
    //         type: 'error',
    //       });
    //     }

    // }
    const errors = error.response.data.message; 
    console.log(errors);
    if (errors) {
      Vue.$toast.open({
        message: errors,
        type: 'error',
      });
    }
    // Any status codes that falls outside the range of 2xx cause this function to trigger
    // Do something with response error
    return Promise.reject(error);
  }
}
