import Vue from 'vue'
import VueRouter from 'vue-router'
//import MainPage from "@/views/MainPage.vue";
import MainUser from "@/views/MainUser.vue";
import OnePost from "@/views/OnePost.vue";
import SignUp from "@/views/SignUp.vue";
import MainContentPage from "@/views/MainContentPage.vue";
import AddPostPage from "@/views/AddPostPage.vue";
import ProfilePage from "@/views/ProfilePage.vue";

Vue.use(VueRouter)

const routes = [
  {
    path: '/',
    name:'m',
    component: MainContentPage
  },
  {
    path: '/onePost',
    name:'onePost',
    component: OnePost
  },
  {
    path: '/signUp',
    name: 'signUp',
    component: SignUp
  },
  {
    path: '/user',
    name: 'user',
    component: MainUser,
    children:[
      {
        path: 'newPost',
        name:'newPost',
        component: AddPostPage
      },
      {
        path: 'profile',
        name: 'profile',
        component: ProfilePage
      },
    ],
  },
]

const router = new VueRouter({
  mode: 'history',
  base: process.env.BASE_URL,
  routes
})

export default router
