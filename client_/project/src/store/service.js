import axios from "axios";

export const services = {
   async login(account){
      return await axios.post('/system_management/login' ,account )  
   },
   async signup(account){
      console.log(account);
      return await axios.post('/system_management/register/' , account)  
   },
   async fetchAllPosts(){
      return await axios.post('/system_management/posts')
   }
}