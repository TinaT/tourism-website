import Vue from 'vue'
import Vuex from 'vuex'
// import { services } from './service';
//import axios from 'axios';

Vue.use(Vuex)

export default new Vuex.Store({
  state: {
    isUser: false,
    account: {},
    allPosts: [],
    selectedPost: {},
    uploadedImages: [],
    selectedProfile: {}
  },
  mutations: {
    login(state, account) {
      state.account = account;
      state.isUser = true;
    },
    signup(state, account) {
      state.account = account;
      state.isUser = true;
    },
    fetchAllPosts(state, params) {
      state.allPosts = params
    },
    setState(state, paylaod) {
      state[paylaod.key] = paylaod.value;
    },
    uplaodImage(state, payload) {
      state.uploadedImages.push(payload);
    }
  },
  actions: {
    setState({ commit }, payload) {
      commit("setState", payload);
    },
    login({ commit }, account) {
      commit('login', account)
      // commit('login', services.login(account))
    },
    signup({ commit }, account) {
      commit('signup', account)
    },
    fetchAllPosts({ commit }, allPosts) {
      commit("fetchAllPosts", allPosts)
    },
    uplaodImage({ commit }, payload) {
      commit("uplaodImage", payload)
    }
  },
  modules: {
  }
})
