import Vue from 'vue'
import App from './App.vue'
import './registerServiceWorker'
import router from './router'
import store from './store'
import { BootstrapVue } from 'bootstrap-vue'
import 'bootstrap/dist/css/bootstrap.css'
import 'bootstrap-vue/dist/bootstrap-vue.css'
import 'bootstrap/dist/js/bootstrap.min.js'
import VueCarousel from 'vue-carousel';

import VueToast from 'vue-toast-notification';
// Import one of the available themes
//import 'vue-toast-notification/dist/theme-default.css';
import 'vue-toast-notification/dist/theme-sugar.css';
 
Vue.use(VueToast);

// set default config




import { ValidationObserver, ValidationProvider } from 'vee-validate';

Vue.component('ValidationProvider', ValidationProvider);
Vue.component('ValidationObserver', ValidationObserver);


// register jw pagination component globally
import JwPagination from 'jw-vue-pagination';
Vue.component('jw-pagination', JwPagination);

import VueLazyload from 'vue-lazyload'
Vue.use(VueLazyload)

import VueUploadMultipleImage from 'vue-upload-multiple-image'
export default {
  components: {
    VueUploadMultipleImage,
  },
}

import VueCookies from 'vue-cookies'
Vue.use(VueCookies)

Vue.$cookies.config('12h');

// set global cookie
Vue.$cookies.set('theme','default');
Vue.$cookies.set('hover-time','1s');

import AOS from "aos";
import "aos/dist/aos.css";
AOS.init();

import './axios/index';


require('@/assets/vendor/bootstrap/css/bootstrap.min.css')
require('@/assets/vendor/icofont/icofont.min.css')
require('@/assets/vendor/boxicons/css/boxicons.min.css')
require('@/assets/vendor/remixicon/remixicon.css')
require('@/assets/vendor/aos/aos.css')

Vue.use(BootstrapVue)
Vue.use(VueCarousel)
Vue.config.productionTip = false

new Vue({
  router,
  store,
  render: h => h(App)
}).$mount('#app')

