module.exports = {
    devServer: {
      proxy: {
        '/system_management': {
          target: 'http://185.81.96.129:8080',
          ws: true,
          changeOrigin: true
        },
        '/posts_management': {
          target: 'http://185.81.96.129:8080',
          ws: true,
          changeOrigin: true
        }
      }
    },
  };
  